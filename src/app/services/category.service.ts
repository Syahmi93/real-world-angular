import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { Category } from '../model/category.model';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  private _serviceUrl = 'http://localhost:3000/categories';  // URL to web api

  categories: Category[];
  
  constructor(private http: HttpClient) { }

  getCategories(): Observable<Category[]> {
    let url = this._serviceUrl;
    return this.http.get(url)
                .pipe(
                  map<any, Category[]>((val: any) => val ),
                );   
  }
}
