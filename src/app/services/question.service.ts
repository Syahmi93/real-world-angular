import { Injectable } from '@angular/core';
import { Observable, forkJoin } from 'rxjs';
import { map, subscribeOn } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { CategoryService } from './category.service';
import { Question } from '../model/question.model';
import { Category } from '../model/category.model';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  private _questionsUrl = "http://localhost:3000/questions";
  private _categoriesUrl = "http://localhost:3000/categories";

  constructor(private http: HttpClient, private categoryService: CategoryService ) { }

  getQuestions(): Observable<Question[]> {
    let url = this._questionsUrl;
    return forkJoin(
      this.http.get(url)
              .pipe(
                map<any, Question[]>((val: any) => val), 
              ),
      this.categoryService.getCategories())
              .pipe(
                map((combined, index) => {
                  let questions: Question[] = combined[0];
                  let categories: Category[] = combined[1];
                  questions.forEach(q => {
                    q.categories = [];                                                                         
                    q.categoryIds.forEach(id => q.categories.push(categories.find((element: any) => element.id == id)))
                  })
                  return questions;
                  }
                )
              );
  }
}
