import { Component, OnInit } from '@angular/core';
import { QuestionService } from 'src/app/services/question.service';
import { Question } from '../../model/question.model';
import { Category } from '../../model/category.model';

@Component({
  selector: 'question-list',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {

  questions: Question[];
  categories: Category[];
  sub: any;
  constructor(private questionService: QuestionService) { }
  
  ngOnInit() {
    return this.questionService.getQuestions()
    .subscribe(questions => this.questions = questions);
  }

}
