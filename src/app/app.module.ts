import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CategoryComponent } from './components/category/category.component';
import { QuestionComponent } from './components/question/question.component';
import { TagComponent } from './components/tag/tag.component';
import { CategoryService } from './services/category.service';
import { TagService } from './services/tag.service';
import { QuestionService } from './services/question.service';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AppsComponent } from './components/app/app.component';

import {MatSidenavModule} from '@angular/material/sidenav';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatCardModule} from '@angular/material/card';
import {MatListModule, MatNavList} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {MatChipsModule} from '@angular/material/chips';

@NgModule({
  declarations: [
  AppComponent,
  AppsComponent,
  CategoryComponent,
  QuestionComponent,
  TagComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    CommonModule,
    RouterModule,

    MatSidenavModule,
    MatToolbarModule,
    MatCardModule,
    MatListModule,
    MatIconModule,
    MatChipsModule,
    // MatNavList
  ],
  providers: [ CategoryService, TagService, QuestionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
