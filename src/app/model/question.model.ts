import { Category } from './category.model';

export interface Question {
    id: number;
    questionText: string;
    answers: [{
        id: number,
        answerText: string,
        correct: boolean
    }];
    ordered: boolean;
    tags: [];
    categories: Category[];
    categoryIds: [];
}